% generating A(k), B(k) as a function of time-varying perturbation properties
function [A, B] = sys_mats(CASE,k,T,mag_pert)

% common LTI part
A_0 = [1.1 .1;
    0.1 0.2];

B_0 = [0.5 1;
    0.1 0.2];

switch CASE



    case 1


        A=A_0;

        B_1=B_0;

        B_2 = [B_1(1,1) -mag_pert*B_1(1,2);
            B_1(2,1) -mag_pert*B_1(2,2)];



        if ~mod(floor(abs(k-1)/T),2)



            B=B_1;

        else

            B=B_2;

        end



    case 2



        Pert=diag([mag_pert*cos(2*pi*k/T) -mag_pert*cos(2*pi*k/T)]);

        A = A_0*(eye(2)+Pert);

        B = B_0;




end


