% hybrid systems data-based adaptive control core routine
function HS_DAC(CASE,CONTROLLER);

% Define parameters of time-varying perturbations

switch CASE

    case 1
        %% Switching plant

        % Specify:

        % - period of the switch
        period= 12; % option analyzed in the paper: 12

        % - amplitude of the perturbation (input matrix column)
        magnitude = 1; % options analyzed in the paper: 1; 2.5


        % options needed for the comparison with time-triggered controller
        % period of the update
        trigger_period=12; % options analyzed in the paper: 8;12;16 (for mag=1)

        % offset in the triggering instant
        bias_T=4; % options analyzed in the paper: 0;4 (for mag=1)


    case 2
        %% Sinusoidal variations

        % Specify:

        % - period of the sinusoid
        period= 10; % options analyzed in the paper: 8,10,12

        % - amplitude of the perturbation
        magnitude = 0.8; % option analyzed in the paper: 0.8

        % options needed for the comparison with time-triggered controller (not
        % done in the paper for this CASE!)

        % period of the update
        trigger_period=15; % options analyzed in the paper: 8;12;16

        % offset in the triggering instant
        bias_T=0; % options analyzed in the paper: 0;4


end

% Getting nx and nu to initialize variables
k = 1; %dummy choice, just to get matrices sizes
[A_0, B_0] = sys_mats(CASE,k,period,magnitude);
nx=size(A_0,1); nu=size(B_0,2);


% Offline data collection parameters to initialize data matrices
T_off = nx + nu ; % length of offline design (same as #columns of data-matrices online)

k_off = 1 : T_off; % timestep offline

m_off = 1; % magnitude of the offline input

u_off = m_off .* rand(nu,T_off); % random uniform excitation
%load('C:\Users\andre\Desktop\MATLAB\DD_adaptive_LTV\results\May\Sine1_T_p8.mat','u_off')

x_0=zeros(nx,1); % zero initial condition

% Initialize data-matrices
X_0=zeros(nx,T_off); X_1=X_0;
U_0=zeros(nu,T_off);


%% Offline experiment (starting at k=0)

x = x_0;

for k = 1 : T_off

    u = u_off(:,k);
    x_h(:,k)=x; u_h(:,k)=u;

    [A, B] = sys_mats(CASE,k,period,magnitude);

    A_h{k}=A;
    B_h{k}=B;

    x1 = A * x + B * u;

    X_0(:,k)=[x];
    X_1(:,k)=[x1];
    U_0(:,k)=[u];

    x=x1;

end

x_f_off=x1; % last state of the data-collection phase (=initial condition of the controlled phase)

Z_0=[U_0;X_0];

% First gain designed based on the open-loop collected data
[K_0, S_0, F_0, a_1_0, a_2_0] = Map_L(U_0,X_0,X_1);


%% Online phase

T_online = 100; % total simulated time online
columns_data=T_off; % #columns of data-matrices online
% define online data-matrices (initialized at the off-line value)
X_0_ON(:,1:T_off)=X_0; X_1_ON(:,1:T_off)=X_1; U_0_ON(:,1:T_off)=U_0;


% triggering condition

switch CONTROLLER

    case 0

        disp('No triggering')

    case 1

        disp('Event-triggered')

    case 2

        disp('Periodically-triggered')

end


cnt_trig=0;
simga_tol=0.1;

K = K_0; S = S_0; a=a_1_0; F=F_0;
sigma=1-simga_tol*(1-a); % needed for triggering condition

x=x_f_off; % initial condition online phase

tol_state = 5*10^(-4); % to avoid numerical issues, triggering only takes place when state norm is smaller than this


for k = T_off + 1 : T_online % finite-horizon where problem is solved (starting from the last offline instant until T_online)

    u = K*x;

    x_h(:,k)=x; u_h(:,k)=u;

    [A, B] = sys_mats(CASE,k,period,magnitude);

    A_h{k}=A; B_h{k}=B;

    x1 = A * x + B * u;

    % saving old data matrices
    X_0_ON_old=X_0_ON;X_1_ON_old=X_1_ON;U_0_ON_old=U_0_ON;

    % updating data matrices with new data
    X_0_ON(:,1)=[x]; X_1_ON(:,1)=[x1]; U_0_ON(:,1)=[u];
    X_0_ON(:,2:columns_data)=X_0_ON_old(:,1:end-1); X_1_ON(:,2:columns_data)=X_1_ON_old(:,1:end-1); U_0_ON(:,2:columns_data)=U_0_ON_old(:,1:end-1);

    Z_0=[U_0_ON;X_0_ON];


    % Controller update

    if CONTROLLER==1

        % Checking event (Lyapunov condition not satisfied and state norm above threshold)

        if x1'*S*x1-sigma*x'*S*x >0 && norm(x1,2)>tol_state
            cnt_trig = cnt_trig + 1;
            idx_trig(cnt_trig,1)=k


            %[K_i, S_i, a_i, F_i] = K_Proposition2_NEW(U_0_ON,X_0_ON,X_1_ON);
            [K_i, S_i, F_i, a_1_i, a_2_i] = Map_L(U_0_ON,X_0_ON,X_1_ON);

            if ~isempty(K_i)
                feas_design(cnt_trig,1)=1;

                K=K_i;S=S_i;a=a_1_i; sigma=1-simga_tol*(1-a_1_i); F=F_i;
                a_hist(cnt_trig,1)=a;
                S_norm(cnt_trig,1)=norm(S,2);

            else % map L is empty. Controller is not updated

                feas_design(cnt_trig,1)=0;

            end
            K_h{cnt_trig}=K;


        end


    elseif CONTROLLER==2

        % Periodically-triggered update

        % A new controller is designed with period "trigger_period" and bias
        % "trigger_bias", as explained in the paper


        if k>trigger_period && (floor( (k-(bias_T+columns_data+1))/trigger_period) >= (k-(bias_T+columns_data+1))/trigger_period) %&& norm(x1,2)>tol_state %&& norm(x1,2)>tol_state


            cnt_trig = cnt_trig + 1;
            idx_trig(cnt_trig,1)=k;
            [K_i, S_i, F_i, a_1_i, a_2_i] = Map_L(U_0_ON,X_0_ON,X_1_ON);

            if ~isempty(K_i)
                K=K_i;S=S_i;a=a_1_i;
                feas_design(cnt_trig,1)=1;
            else
                feas_design(cnt_trig,1)=0;

            end
            K_h{cnt_trig}=K;

        end

    end

    x=x1;

end




end





