clear all
clc
cd('C:\Users\andre\Desktop\MATLAB\DD_adaptive_LTV\repo')
%% master file to reproduce results of paper
% "A hybrid systems framework for data-based adaptive control of linear time-varying systems"
% seeding randomness for reproducibility and debug
rng('default');
s = rng;

% Two simulation scenarios:

CASE = 1; % 1 (switching), 2 (sinusoidal)

% Controller options
% 0 (time-invariant controller designed with offline data), 
% 1 (proposed algorithm: event-triggered adaptive controller), 
% 2 (time-triggered adaptive controller)->for this, choose inside HS_DAC the period and starting time  

CONTROLLER=2; 

switch CASE

    case 1
%% Switching plant

HS_DAC(CASE,CONTROLLER);

    case 2
%% Sinusoidal variations

HS_DAC(CASE,CONTROLLER);
   
end




